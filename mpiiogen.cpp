#include <mpi.h>
#include <functional>
#include <algorithm>
#include <random>

int main(int argc, char ** argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  MPI_File out;
  MPI_File_open(MPI_COMM_WORLD, argv[1], MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &out);

  MPI_Offset start, end;
  auto filesize = atoi(argv[2]);
  start = (filesize / size) * rank;
  end = (filesize / size) * (rank + 1);
  if(rank == size - 1)
  {
    end += filesize % size;
  }
  auto data = new char[end - start];
  auto dice = std::bind(
    std::uniform_int_distribution< char >{48, 57},
    std::default_random_engine{}
    );
  std::generate_n(data, end - start, [&]
		  {
		    return dice();
		  });
  MPI_File_write_at_all(out, start, data, end - start, MPI_CHAR, MPI_STATUS_IGNORE);
  MPI_Finalize();
  delete [] data;
  return 0;
}
