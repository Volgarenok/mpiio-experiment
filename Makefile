mpiex: mpiioex.cpp
	mpic++ $^ -o $@

mpigen: mpiiogen.cpp
	mpic++ $^ -o $@

mpistat: status.cpp
	mpic++ $^ -o $@

run: mpiex
	mpirun -n 2 ./mpiex example.txt example.out.txt

gen: mpigen
	mpirun -n 2 ./mpigen data.txt 1048576

stat: mpistat
	mpirun -n 2 ./mpistat

clean:
	@rm mpigen mpiex
