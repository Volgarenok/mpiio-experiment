#include <mpi.h>
#include <omp.h>

struct desc
{
  int rank, size, threads;
};

int main(int argc, char ** argv)
{
  auto d = desc{};
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &(d.rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(d.size));
  d.threads = omp_get_max_threads();
  std::cout << "(:rank " << d.rank;
  std::cout << ":size " << d.size;
  std::cout << ":threads " << d.threads;
  std::cout << ":)" << std::endl;
  MPI_Finalize();
  return 0;
}
