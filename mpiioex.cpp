#include <mpi.h>

int main(int argc, char ** argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  MPI_File in, out;
  MPI_File_open(MPI_COMM_WORLD, argv[1], MPI_MODE_RDONLY, MPI_INFO_NULL, &in);

  MPI_Offset start, end, filesize;
  MPI_File_get_size(in, &filesize);
  start = (filesize / size) * rank;
  end = (filesize / size) * (rank + 1);
  if(rank == size - 1)
  {
    end += filesize % size;
  }
  auto data = new char[end - start];
  MPI_File_read_at_all(in, start, data, end - start, MPI_CHAR, MPI_STATUS_IGNORE);
  MPI_File_open(MPI_COMM_WORLD, argv[2], MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &out);
  MPI_File_write_at_all(out, start, data, end - start, MPI_CHAR, MPI_STATUS_IGNORE);
  MPI_Finalize();
  delete [] data;
  return 0;
}
